{{- range . }}
| Package | Vulnerability ID | Severity | Installed Version | Fixed Version | Links |
|---------|------------------|----------|-------------------|---------------|-------|
{{- range .Vulnerabilities }}
| {{ escapeXML .PkgName }} | `{{ escapeXML .VulnerabilityID }}` | {{ escapeXML .Vulnerability.Severity }} | {{ escapeXML .InstalledVersion }} | {{ escapeXML .FixedVersion }} | {{- range .Vulnerability.References -}}[{{ escapeXML . }}]({{ escapeXML . }})<br>{{- end -}} |
{{- end }}
{{- end }}
