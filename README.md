# sbom-web-repository

Git repository to store [SBOM](https://github.com/CycloneDX/bom-examples/tree/master/SBOM) files and display SBOM resources in a static web pages.

It uses sbom-utility to generate markdown configuration and mkdocs to generate web page from markdown files.

Client projects can push there bom files to this git repository which generate the documentation.

[Demo website](https://sbom-web-repository-demo.gitlab.io/sbom-web-repository/)

> Pipeline can be scheduled in order to continuously update the data.